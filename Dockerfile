FROM debian:stretch AS build-env

MAINTAINER <rynojvr>

RUN apt-get update && \
    apt-get install -y gcc libcap-dev libc6-dev libssl-dev ca-certificates git make dh-autoreconf

WORKDIR /root

RUN git clone http://github.com/ntop/n2n && \
    cd n2n && \
    ./autogen.sh && \
    ./configure && \
    make

FROM debian:stretch
COPY --from=build-env /root/n2n/supernode /usr/sbin/
COPY --from=build-env /root/n2n/edge /usr/sbin/
COPY --from=build-env /root/n2n/packages/etc/n2n/* /n2n/

